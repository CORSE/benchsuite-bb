#!/usr/bin/env python3

from setuptools import setup, find_packages
import sys


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-") or line.startswith("git+"):
                continue
            reqs.append(line)
    return reqs


setup(
    name="benchsuite_bb",
    version="0.0.1",
    description="Extract basic blocks from various benchmark suites",
    author="CORSE",
    license="LICENSE",
    packages=find_packages(),
    include_package_data=True,
    package_data={"benchsuite_bb": ["py.typed"]},
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={
        "console_scripts": [
            ("benchsuite-bb-spec2017 = benchsuite_bb.entrypoint:main_spec2017"),
            ("benchsuite-bb-polybench = benchsuite_bb.entrypoint:main_polybench"),
            ("benchsuite-bb-rodinia = benchsuite_bb.entrypoint:main_rodinia"),
        ]
    },
)
