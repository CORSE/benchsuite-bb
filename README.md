# Benchsuite BB

This module aims to extract relevant basic blocks from various benchmark
suites, including so far:

* SPEC2017
* Polybench
* Rodinia

along with frequency of execution. For this purpose, the benchmark suite
has to be run inside a profiler (here, Linux `perf`).

The following other benchmark suites are planned:

* MediaBench

Note that this repository does **not** include the SPEC2017 benchmark suite, as
it is not FOSS software: you should have your own copy installed on the machine
you wish to use.
