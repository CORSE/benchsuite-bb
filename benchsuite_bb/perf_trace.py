""" Analyze a `perf report -D` trace. Original code by Christophe Guillon, adapted. """

# Example:
# if the loader mmaps the program at 0x1000000 and there is a sample at 0x1000010, the
# result will be a sample 0x10, and thus a line with '16'

import logging
import os
import re
import subprocess
import sys
import typing as t
from pathlib import Path

from elftools.elf.elffile import ELFFile
from elftools.elf.segments import Segment as ELFSegment

logger = logging.getLogger(__name__)

# Format
# PERF_RECORD_MMAP2 9798/9798: [0x7ffed19fc000(0x2000) @ 0 00:00 0 0]: r-xp /some/file
#                               ^ load addr    ^ size    ^ offset
# PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x555555554626
# PERF_RECORD_EXIT(20338:20338):(20338:20338)


class Segment(t.NamedTuple):
    """An ELF segment"""

    addr: int
    size: int


class PerfTraceDecoder:
    _sample_re = re.compile(
        r"PERF_RECORD_(?P<type>SAMPLE)[^:]+:[^:]+: 0x(?P<addr>[0-9a-f]+)"
    )
    _mmap_re = re.compile(
        r"PERF_RECORD_(?P<type>MMAP2) (?P<pid>[0-9]+)/(?P<pid2>[0-9]+): \[0x(?P<addr>[0-9a-f]+)\(0x(?P<size>[0-9a-f]+)\) @ (?P<offset>(0x)?[0-9a-z]+) [^]]*\]: [^ ]+ (?P<file>.+)"
    )
    _exit_re = re.compile(
        r"PERF_RECORD_(?P<type>EXIT)\((?P<pid>[0-9]+):(?P<pid2>[0-9]+)\)"
    )

    exe_fname: Path
    segments: list[Segment]

    def __init__(self, binary: Path):
        self.exe_fname = binary.resolve()
        self.segments = self._get_loadables()

    @classmethod
    def new_bare(cls) -> "PerfTraceDecoder":
        """Instantiate this class without any ELF and segments. For unit tests."""
        out: "PerfTraceDecoder" = cls.__new__(cls)
        out.exe_fname = Path("/some/file")
        out.segments = []
        return out

    def _get_loadables(self) -> list[Segment]:
        with self.exe_fname.open("rb") as handle:
            elffile = ELFFile(handle)
            segments: list[ELFSegment] = list(elffile.iter_segments())
            segments.sort(key=lambda seg: seg.header.p_vaddr)
            return [
                Segment(seg.header.p_offset, seg.header.p_memsz)
                for seg in filter(
                    lambda seg: seg.header.p_type == "PT_LOAD"
                    and seg.header.p_memsz > 0,
                    segments,
                )
            ]

    def trace_from_perfdata(self, perfdata: Path, perf_bin: str = "perf") -> str:
        try:
            result = subprocess.run(
                [
                    perf_bin,
                    "report",
                    "-d",
                    self.exe_fname,
                    "-i",
                    perfdata.resolve(),
                    "-D",
                ],
                capture_output=True,
                text=True,
                check=True,
            )
        except subprocess.CalledProcessError as exn:
            logger.critical(
                "Failed to analyse perf data (status code %d). Output:\n%s",
                exn.returncode,
                exn.output,
            )
            raise exn from exn

        return result.stdout

    class ExeData(t.NamedTuple):
        addr: int
        offset: int
        size: int
        pid: int

    def analyze_trace(self, trace: str) -> list[int]:
        exe: t.Optional["PerfTraceDecoder.ExeData"] = None
        samples = []

        for line in trace.split("\n"):
            if exe is None:
                match = self.__class__._mmap_re.search(line)
                if match:
                    file = match.group("file")
                    if file != str(self.exe_fname):
                        continue
                    offset_str = match.group("offset")
                    if offset_str == "0":
                        offset = 0
                    else:
                        assert offset_str.startswith("0x")
                        offset = int(offset_str[2:], 16)
                    exe = self.ExeData(
                        addr=int(match.group("addr"), 16),
                        offset=offset,
                        size=int(match.group("size"), 16),
                        pid=int(match.group("pid")),
                    )
                    logger.debug(
                        "PERF_RECORD_%s: %s/%s: 0x%s (0x%s bytes at offset %s): %s",
                        match.group("type"),
                        match.group("pid"),
                        match.group("pid2"),
                        match.group("addr"),
                        match.group("size"),
                        match.group("offset"),
                        match.group("file"),
                    )
            else:
                match = self.__class__._exit_re.search(line)
                if match:
                    pid = int(match.group("pid"))
                    if pid != exe.pid:
                        continue
                    logger.debug(
                        "PERF_RECORD_%s: %s/%s",
                        match.group("type"),
                        match.group("pid"),
                        match.group("pid2"),
                    )
                    exe = None
            if exe is not None:
                match = self.__class__._sample_re.search(line)
                if match:
                    addr = int(match.group("addr"), 16)
                    if 0 <= addr - exe.addr < exe.size:
                        for seg in self.segments:
                            seg_raddr = seg.addr + exe.addr - exe.offset
                            if seg_raddr <= addr < seg_raddr + seg.size:
                                sample = addr - exe.addr + exe.offset
                                samples.append(sample)
                                break

        return samples
