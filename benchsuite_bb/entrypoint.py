import argparse
import logging
import os
import typing as t
from pathlib import Path

from tbastian_pyutils.messages import setup_logging

from . import extract_bb
from .bench_state import OutputState
from .suite_specific import polybench, rodinia, spec2017

logger = logging.getLogger(__name__)


def common_args(parser: argparse.ArgumentParser):
    """Add common arguments to a parser"""
    parser.add_argument(
        "--outdir",
        required=True,
        help="Path at which resulting basic blocks should be stored",
    )
    parser.add_argument(
        "--force-rerun",
        action="store_true",
        help=(
            "Rerun all benchmarks, even if matching saved data is available from "
            "previous runs"
        ),
    )
    parser.add_argument(
        "--filter-absolute",
        type=int,
        help="Only keep basic blocks with more than this number of occurrences",
    )
    parser.add_argument(
        "--filter-relative",
        type=float,
        help=(
            "Only keep basic blocks accounting for at least this percentage of the "
            "occurrences"
        ),
    )
    parser.add_argument(
        "--as-tb", action="store_true", help="Output the basic blocks as TB format"
    )
    parser.add_argument(
        "--as-yaml", action="store_true", help="Output the basic blocks as yaml format"
    )

    parser.add_argument(
        "-g",
        "--debug",
        dest="loglevel",
        default=logging.INFO,
        action="store_const",
        const=logging.DEBUG,
    )


def common_parser() -> argparse.ArgumentParser:
    """Create the base parser"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog="""environment variables:
  PERF_BIN: perf binary to use (default: 'perf')""",
    )
    common_args(parser)
    return parser


def basic_init(args) -> str:
    setup_logging(args.loglevel)

    perf_bin = os.environ.get("PERF_BIN", "perf")

    return perf_bin


def make_output_state(args) -> OutputState:
    """Make output state out of common command line arguments"""

    outdir = Path(args.outdir).resolve()
    outdir.mkdir(parents=True, exist_ok=True)

    return OutputState(
        out_dir=outdir,
        dump_tb=args.as_tb,
        dump_yml=args.as_yaml,
        filter_absolute=args.filter_absolute,
        filter_relative=args.filter_relative,
    )


def main_spec2017():
    """Entrypoint function for SPEC2017"""
    parser = common_parser()

    parser.add_argument(
        "--suite", default="intspeed", help="Select the SPEC2017 suite to run"
    )
    parser.add_argument(
        "--size",
        choices=spec2017.SPEC_SIZES,
        default="test",
        help="Select the SPEC2017 input size",
    )
    parser.add_argument(
        "--config", required=True, help="Select the SPEC2017 configuration to use"
    )
    parser.add_argument(
        "--specroot", default=".", help="Path to the install directory of SPEC2017"
    )
    args = parser.parse_args()

    perf_bin = basic_init(args)

    state = spec2017.SpecState(
        root=Path(args.specroot).resolve(strict=True),
        force_rerun=args.force_rerun,
        perf_bin=perf_bin,
        spec_suite=args.suite,
        spec_size=args.size,
        spec_conf=args.config,
    )
    output_state = make_output_state(args)

    extract_bb.extract_all(
        state=state,
        output_state=output_state,
    )


def main_polybench():
    """Entrypoint function for Polybench"""
    parser = common_parser()

    parser.add_argument(
        "--size",
        default="MEDIUM",
        help="Select the Polybench input size",
        choices=polybench.SUITE_SIZES,
    )
    parser.add_argument(
        "--cflags", default="", help="Additional flags to pass to the C compiler"
    )
    parser.add_argument(
        "--force-rebuild",
        action="store_true",
        help="Rebuild even if polybench binaries are already built",
    )
    parser.add_argument(
        "--polyroot", default=".", help="Path to the install directory of Polybench"
    )
    parser.add_argument(
        "--limit",
        "-l",
        help="Comma-separated list of benchmarks. Only those will be used.",
    )
    args = parser.parse_args()

    perf_bin = basic_init(args)

    limit: t.Optional[list[str]] = None
    if args.limit:
        limit = list(map(lambda x: x.strip(), args.limit.split(",")))
    state = polybench.PolybenchState(
        root=Path(args.polyroot).resolve(strict=True),
        perf_bin=perf_bin,
        force_rerun=args.force_rerun,
        polybench_size=args.size,
        polybench_cflags=args.cflags,
        polybench_rebuild=args.force_rebuild,
        polybench_limit=limit,
    )
    output_state = make_output_state(args)

    extract_bb.extract_all(
        state=state,
        output_state=output_state,
    )


def main_rodinia():
    """Entrypoint function for Rodinia"""
    parser = common_parser()

    parser.add_argument(
        "--root", default=".", help="Path to the install directory of Rodinia"
    )
    parser.add_argument(
        "--size",
        default="MEDIUM",
        help="Select the Rodinia input size",
        choices=rodinia.SUITE_SIZES,
    )
    parser.add_argument(
        "--limit",
        "-l",
        help="Comma-separated list of benchmarks. Only those will be used.",
    )
    parser.add_argument(
        "--use-mummergpu",
        help="Enable the mummergpu benchmark. This benchmark easily fails to build.",
        action="store_true",
    )
    args = parser.parse_args()

    perf_bin = basic_init(args)

    limit: t.Optional[list[str]] = None
    if args.limit:
        limit = list(map(lambda x: x.strip(), args.limit.split(",")))
    state = rodinia.RodiniaState(
        root=Path(args.root).resolve(strict=True),
        perf_bin=perf_bin,
        rodinia_limit=limit,
        rodinia_size=args.size,
        rodinia_use_mummergpu=args.use_mummergpu,
    )
    output_state = make_output_state(args)

    extract_bb.extract_all(
        state=state,
        output_state=output_state,
    )
