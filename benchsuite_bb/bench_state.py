""" State that is acquired while initializing/benchmarking """

import typing as t
from dataclasses import dataclass, field
from pathlib import Path


class PerfRecordDetails(t.NamedTuple):
    binary: Path
    record_file: Path
    saved_record_file: Path


@dataclass
class SuiteState:
    root: Path
    perf_bin: str
    need_disasm: bool = False
    force_rerun: bool = False
    benchmarks: dict[int, str] = field(default_factory=dict)
    bench_rundir: dict[int, str] = field(default_factory=dict)
    bench_perf_details: dict[int, list[PerfRecordDetails]] = field(default_factory=dict)

    @property
    def perf_prefix(self):
        return f"{self.perf_bin} record -o {{perf_file}} linux64 -R -- "


@dataclass
class OutputState:
    out_dir: Path
    dump_tb: bool
    dump_yml: bool
    filter_absolute: t.Optional[int] = None
    filter_relative: t.Optional[float] = None

    @property
    def need_disasm(self):
        """Does this output require keeping a disassembly of blocks?"""
        return self.dump_tb
