""" Handle the parts specific to the Polybench suite """

import logging
import re
import shlex
import subprocess
import typing as t
from dataclasses import dataclass, field
from pathlib import Path

from ..bench_state import PerfRecordDetails, SuiteState

logger = logging.getLogger(__name__)


class PolybenchException(Exception):
    """Generic Polybench exception"""


class PolybenchBadConfig(PolybenchException):
    """Raised when a bad configuration is given"""


SUITE_SIZES: list[str] = ["MINI", "SMALL", "MEDIUM", "LARGE", "EXTRALARGE"]

_SAVED_STATE_DIR: str = "benchsuite_bb_state/{bench_name}_{size}"


@dataclass
class PolybenchState(SuiteState):
    polybench_size: str = ""
    polybench_cflags: str = ""
    polybench_rebuild: bool = False
    polybench_limit: t.Optional[list[str]] = None

    def _stem_path_of(self, b_id: int) -> Path:
        return self.root / self.bench_rundir[b_id] / self.benchmarks[b_id]

    def bin_path(self, b_id: int) -> Path:
        return self._stem_path_of(b_id).with_suffix(".bin")

    def c_path(self, b_id: int) -> Path:
        return self._stem_path_of(b_id).with_suffix(".c")


def polybench_bench(state: PolybenchState, b_id: int):
    """Run and profile one benchmark"""

    saved_state_dir = _SAVED_STATE_DIR.format(
        bench_name=state.benchmarks[b_id], size=state.polybench_size
    )
    perf_file = "perf.benchsuitebb.data"
    instrument_prefix = shlex.split(state.perf_prefix.format(perf_file=perf_file))
    rundir = state.root / state.bench_rundir[b_id]
    record_details = PerfRecordDetails(
        binary=state.bin_path(b_id),
        record_file=rundir / perf_file,
        saved_record_file=state.root / saved_state_dir / perf_file,
    )
    state.bench_perf_details[b_id] = [record_details]

    if not state.force_rerun and record_details.saved_record_file.exists():
        logger.info(
            "Matching previous profiling file found, skipping %s",
            state.benchmarks[b_id],
        )
        return

    logger.info(
        "Running and profiling benchmark %s…",
        state.benchmarks[b_id],
    )
    try:
        subprocess.run(
            instrument_prefix + [state.bin_path(b_id).as_posix()],
            cwd=rundir.as_posix(),
            capture_output=True,
            text=True,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to profile/run benchmark %s (status code %d). Output: \n%s",
            state.benchmarks[b_id],
            exn.returncode,
            exn.stderr,
        )
        raise PolybenchException(
            f"Could not profile/run benchmark {state.benchmarks[b_id]}."
        ) from exn

    # Saving profiling data
    record_details.saved_record_file.parent.mkdir(parents=True, exist_ok=True)
    record_details.record_file.rename(record_details.saved_record_file)


def polybench_build(state: PolybenchState, b_id: int):
    """Build one benchmark"""
    rundir = state.root / state.bench_rundir[b_id]

    utilities = state.root / "utilities"
    polybench_cflags = [
        f"-D{state.polybench_size}_DATASET",
        "-I.",
        f"-I{utilities}",
        "-lm",
        (state.root / "utilities/polybench.c").as_posix(),
    ] + shlex.split(state.polybench_cflags)

    try:
        subprocess.run(
            [
                "gcc",
                "-o",
                state.bin_path(b_id).as_posix(),
                state.c_path(b_id).as_posix(),
            ]
            + polybench_cflags,
            capture_output=True,
            cwd=rundir.as_posix(),
            text=True,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to compile benchmark %s (status code %d). Output: \n%s",
            state.benchmarks[b_id],
            exn.returncode,
            exn.stderr,
        )
        raise PolybenchException(
            f"Could not build benchmark {state.benchmarks[b_id]}."
        ) from exn

    if not state.bin_path(b_id).exists():
        raise PolybenchException(
            f"Could not build benchmark {state.benchmarks[b_id]} (no binary produced)"
        )


def polybench_init(state: PolybenchState):
    """Initialize Polybench state"""
    if state.polybench_size not in SUITE_SIZES:
        raise PolybenchBadConfig(
            "The provided size %s is not a valid size.", state.polybench_size
        )

    # Load benchmark list
    all_stems: list[Path] = []
    with (state.root / "utilities/benchmark_list").open("r") as handle:
        for line in handle:
            line = line.strip()
            path = Path(line)
            assert path.suffix == ".c"
            all_stems.append(path.with_suffix(""))

    if state.polybench_limit is not None:

        def filter_limit(bench: Path) -> bool:
            assert state.polybench_limit is not None  # mypy doesn't scope correctly
            return bench.stem in state.polybench_limit

        all_stems = list(filter(filter_limit, all_stems))
        logger.info(
            "Limiting benchmarks. Keeping %s.",
            ", ".join(map(lambda bench: bench.stem, all_stems)),
        )

    state.benchmarks = {
        b_id: benchmark.stem for b_id, benchmark in enumerate(all_stems)
    }
    state.bench_rundir = {
        b_id: benchmark.parent.as_posix() for b_id, benchmark in enumerate(all_stems)
    }

    # Build benchmarks
    for b_id in state.benchmarks:
        if state.polybench_rebuild or not state.bin_path(b_id).exists():
            logger.info("Benchmark %s: compiling", state.benchmarks[b_id])
            polybench_build(state, b_id)
        else:
            logger.warning(
                "Benchmark %s: keeping already compiled binary", state.benchmarks[b_id]
            )
