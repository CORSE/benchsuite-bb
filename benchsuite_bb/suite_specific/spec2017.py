import logging
import re
import subprocess
from dataclasses import dataclass, field
from pathlib import Path

from ..bench_state import PerfRecordDetails, SuiteState

logger = logging.getLogger(__name__)


class SpecInitException(Exception):
    """Raised when Spec cannot be initialized"""


class SpecFail(Exception):
    """Raised when some SPEC2017 operation failed"""


SPEC_SIZES: list[str] = ["test", "train", "ref"]

# Example line:
# Error with make.diffwrf_621 'specmake --output-sync --jobs=8 build TARGET=diffwrf_621':
_ERROR_RE = re.compile(r"Error with (?P<task>[^ ]*) '(?P<cmd>[^']*)':")

# Example line:
# *** Error building 627.cam4_s base
_END_ERROR_RE = re.compile(r"\*\*\* Error building (?P<name>[^ ]*) ")

_SAVED_STATE_DIR: str = "benchsuite_bb_state/{bench_id:d}_{size}_{conf}"


@dataclass
class SpecState(SuiteState):
    spec_suite: str = ""
    spec_size: str = ""
    spec_conf: str = ""


def _parse_errors(result_lines: list[str]) -> bool:
    """Parse the output of runcpu runsetup, logs errors, return True if errors were
    found"""

    has_errors: bool = False
    in_error: bool = False
    see_file: str = ""
    command: str = ""
    task: str = ""
    next_is_file = False
    for line in result_lines:
        if not in_error and line.startswith("Error with "):
            match = _ERROR_RE.match(line)
            assert match
            in_error = True
            has_errors = True
            command = match.group("cmd")
            task = match.group("task")
        if in_error:
            if line.strip() == "Please review this file:":
                next_is_file = True
                continue
            if next_is_file:
                see_file = line.strip().strip('"')
                next_is_file = False
                continue
            match = _END_ERROR_RE.match(line)
            if match:
                logger.error(
                    (
                        "ERROR for %s: could not run %s. Failed command is:\n"
                        + "%s\nPlease review %s."
                    ),
                    match.group("name"),
                    task,
                    command,
                    see_file,
                )
                in_error = False
                see_file = ""
                command = ""
                task = ""

    return has_errors


def spec_init(state: SpecState) -> None:
    """Initializes the test suite and setup run directories"""

    try:
        env: dict[str, str] = {}
        result = subprocess.run(
            [
                "./bin/runcpu",
                "--config=" + state.spec_conf,
                "--size=" + state.spec_size,
                "--action=runsetup",
                "--noreportable",
                "--iterations=1",
                "--copies=1",
                state.spec_suite,
            ],
            capture_output=True,
            cwd=state.root.as_posix(),
            text=True,
            env=env,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to runsetup (status code %d). Output:\n%s",
            exn.returncode,
            exn.output,
        )
        raise SpecFail(
            "Could not setup the experiment (runcpu --action=runsetup)."
        ) from exn

    result_lines = result.stdout.split("\n")

    # Find errors
    if _parse_errors(result_lines):
        raise SpecInitException("Could not initialize spec -- errors occurred")

    # Find selected benchmarks' names
    for line in result_lines:
        if line.startswith("Benchmarks selected: "):
            _, benchs_str = line.split(": ", maxsplit=1)
            benchs: list[str] = list(map(lambda x: x.strip(), benchs_str.split(",")))
            break
    else:
        raise SpecFail("No benchmarks selected during runsetup")

    state.benchmarks = {}
    for bench in benchs:
        bench_id: int = int(bench[: bench.index(".")])
        state.benchmarks[bench_id] = bench

    # Find run directory for each benchmark
    state.bench_rundir = {}
    in_block: bool = False
    block_re = re.compile(r"Setting up (?P<bid>[0-9]+)\..*: (?P<dir>.*)$")
    for line_id, line in enumerate(result_lines):
        if line.startswith("Setting Up Run Directories"):
            in_block = True
            continue
        if in_block:
            if line[0] != " ":
                break  # The relevant section is over
            match = block_re.search(line)
            if not match:
                raise SpecFail("Could not parse rundirs in runsetup")
            state.bench_rundir[int(match.group("bid"))] = match.group("dir")


def specinvoke(state: SpecState, b_id: int, rundir: Path) -> str:
    try:
        env: dict[str, str] = {}
        result = subprocess.run(
            [
                (state.root / "bin/specinvoke").as_posix(),
                "-nn",
            ],
            capture_output=True,
            cwd=rundir.as_posix(),
            text=True,
            env=env,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to specinvoke on %s (status code %d). Output: \n%s",
            state.benchmarks[b_id],
            exn.returncode,
            exn.output,
        )
        raise SpecFail(
            f"Could not run specinvoke on benchmark {state.benchmarks[b_id]}."
        ) from exn

    result_lines = result.stdout.strip().split("\n")

    # Remove `specinvoke exit` line
    assert result_lines[-1].startswith("specinvoke exit")
    result_lines = result_lines[:-1]

    # Alter specinvoke output
    out_lines: list[str] = []
    out_lines.append("# Generated by spec2017-extract-bb")
    in_block: bool = False
    run_id: int = 0
    perf_details: list[PerfRecordDetails] = []
    for line in result_lines:
        if line.startswith("# Starting run for copy"):
            in_block = True
            out_lines.append(line)
            continue
        if line.strip() == "":
            in_block = False
            out_lines.append(line)
            continue

        if in_block:
            if line.startswith("cd "):
                out_lines.append(line)
            else:  # Spec run line -- instrument
                perf_file = f"perf.{run_id}.data"
                saved_state_dir = _SAVED_STATE_DIR.format(
                    bench_id=b_id, size=state.spec_size, conf=state.spec_conf
                )
                instrument_prefix = state.perf_prefix.format(perf_file=perf_file)
                run_id += 1
                binary = line.strip().split(" ", maxsplit=1)[0].strip()
                out_lines.append(instrument_prefix + line)

                perf_details.append(
                    PerfRecordDetails(
                        binary=rundir / binary,
                        record_file=rundir / perf_file,
                        saved_record_file=state.root / saved_state_dir / perf_file,
                    )
                )
        else:
            out_lines.append(line)

    state.bench_perf_details[b_id] = perf_details
    return "\n".join(out_lines)


def spec_bench(state: SpecState, b_id: int):
    """Handles all the benchmark runs included in a single benchmark id"""

    bench_root: Path = state.root / "benchspec/CPU" / state.benchmarks[b_id]
    bench_rundir: Path = bench_root / "run" / state.bench_rundir[b_id]

    logger.info("Generating perf record script for %s...", state.benchmarks[b_id])
    bench_script_content: str = specinvoke(state, b_id, bench_rundir)
    bench_script_path = bench_rundir / "perf_record_run.sh"
    with bench_script_path.open("w") as handle:
        handle.write(bench_script_content + "\n")

    # Try to recover previous profiling
    already_profiled = all(
        (
            details.saved_record_file.exists()
            for details in state.bench_perf_details[b_id]
        )
    )
    if not state.force_rerun and already_profiled:
        logger.info(
            "Matching previous profiling files found, skipping %s",
            state.benchmarks[b_id],
        )
        return

    logger.info("Running perf record script for %s...", state.benchmarks[b_id])
    try:
        env: dict[str, str] = {}
        subprocess.run(
            [
                "/bin/bash",
                bench_script_path.as_posix(),
            ],
            cwd=bench_rundir.as_posix(),
            env=env,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to run perf record script for %s (status code %d). Output: \n%s",
            state.benchmarks[b_id],
            exn.returncode,
            exn.output,
        )
        raise SpecFail(
            f"Could not run perf record script on benchmark {state.benchmarks[b_id]}."
        ) from exn

    # Saving profiling data
    for details in state.bench_perf_details[b_id]:
        details.saved_record_file.parent.mkdir(parents=True, exist_ok=True)
        details.record_file.rename(details.saved_record_file)
