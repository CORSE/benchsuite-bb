""" Handle the parts specific to the Rodinia suite """

# Rodinia has much less tooling than other benchmark suites. This file contains more
# hardcoded data than others; hopefully it won't become too brittle.

import logging
import re
import shlex
import subprocess
import typing as t
from contextlib import contextmanager
from dataclasses import dataclass, field
from pathlib import Path

from ..bench_state import PerfRecordDetails, SuiteState

logger = logging.getLogger(__name__)


class RodiniaException(Exception):
    """Generic Rodinia exception"""


class RodiniaBenchException(RodiniaException):
    """Raised when a benchmark from Rodinia fails to run"""


SUITE_SIZES: list[str] = ["SMALL", "MEDIUM", "LARGE"]
_ID_FOR_SIZE: dict[str, int] = {x: id_x for id_x, x in enumerate(SUITE_SIZES)}
_SAVED_STATE_DIR: str = "benchsuite_bb_state/{bench_name}_{size}"

""" Manually configured. Yes, this is brittle. """
_BENCHMARK_DATA_DIR: dict[str, str] = {
    "bfs": "bfs",
    "euler3d_cpu": "cfd",
    "euler3d_cpu_double": "cfd",
    "heartwall": "heartwall",
    "hotspot": "hotspot",
    "kmeans": "kmeans",
    "leukocyte": "leukocyte",
    "nn": "nn",
    "pre_euler3d_cpu": "cfd",
    "pre_euler3d_cpu_double": "cfd",
}

_BENCHMARK_INPUTS: dict[str, tuple[str, str, str]] = {
    "backprop": (
        "10000",
        "1000000",
        "10000000",
    ),
    "bfs": (
        "1 graph4096.txt",
        "1 graph65536.txt",
        "1 graph1MW_6.txt",
    ),
    "euler3d_cpu": (
        "fvcorr.domn.097K",
        "fvcorr.domn.193K",
        "missile.domn.0.2M",
    ),
    "euler3d_cpu_double": (
        "fvcorr.domn.097K",
        "fvcorr.domn.193K",
        "missile.domn.0.2M",
    ),
    "heartwall": (
        "test.avi 2 1",
        "test.avi 10 1",
        "test.avi 20 1",
    ),
    "hotspot": (
        "64 64 2 1 temp_64 power_64 /dev/null",
        "512 512 2 1 temp_512 power_512 /dev/null",
        "1024 1024 2 1 temp_1024 power_1024 /dev/null",
    ),
    "kmeans": (
        "-n 1 -i 100",  # yes, there is no .txt here.
        "-n 1 -i 204800.txt",
        "-n 1 -i 819200.txt",
    ),
    "lavaMD": (
        "-cores 1 -boxes1d 2",
        "-cores 1 -boxes1d 5",
        "-cores 1 -boxes1d 13",
    ),
    "leukocyte": (
        "1 1 testfile.avi",
        "3 1 testfile.avi",
        "6 1 testfile.avi",
    ),
    "lud_omp": (
        "-n 1 -s 1024",
        "-n 1 -s 4096",
        "-n 1 -s 8192",
    ),
    "needle": (
        "5000 10 1",
        "20000 10 1",
        "30000 10 1",
    ),
    "nn": (
        "./filelist.txt 3000 30 90",
        "./filelist.txt 30000 30 90",
        "./filelist.txt 80000 30 90",
    ),
    "particle_filter": (
        "-x 128 -y 128 -z 10 -np 10000",
        "-x 128 -y 128 -z 10 -np 50000",
        "-x 128 -y 128 -z 10 -np 100000",
    ),
    "pathfinder": (
        "100000 100",
        "500000 100",
        "100000 1000",
    ),
    "pre_euler3d_cpu": (
        "fvcorr.domn.097K",
        "fvcorr.domn.193K",
        "missile.domn.0.2M",
    ),
    "pre_euler3d_cpu_double": (
        "fvcorr.domn.097K",
        "fvcorr.domn.193K",
        "missile.domn.0.2M",
    ),
    "sc_omp": (
        "10 20 256 1000 1000 100 none /dev/null 1",
        "10 20 256 10000 10000 500 none /dev/null 1",
        "10 20 256 30000 30000 1000 none /dev/null 1",
    ),
    "srad_v1": (
        "100 0.5 502 458 1",
        "500 0.5 502 458 1",
        "2000 0.5 502 458 1",
    ),
    "srad_v2": (
        "128 128 0 31 0 31 0 0.5 500",
        "256 256 12 100 230 124 0 0.5 2000",
        "512 512 12 132 145 500 1 0.5 2000",
    ),
}


@dataclass
class RodiniaState(SuiteState):
    rodinia_limit: t.Optional[list[str]] = None
    rodinia_size: str = ""
    rodinia_use_mummergpu: bool = False

    def run_path(self, b_id: int) -> Path:
        bench = self.benchmarks[b_id]
        if bench in _BENCHMARK_DATA_DIR:
            return self.root / "data" / _BENCHMARK_DATA_DIR[bench]
        return self.root

    def bin_path(self, b_id: int) -> Path:
        return self.root / "bin/linux/omp" / self.benchmarks[b_id]


def rodinia_bench(state: RodiniaState, b_id: int):
    """Run and profile one benchmark"""

    saved_state_dir = _SAVED_STATE_DIR.format(
        bench_name=state.benchmarks[b_id], size=state.rodinia_size
    )
    perf_file = "perf.benchsuitebb.data"
    instrument_prefix = shlex.split(state.perf_prefix.format(perf_file=perf_file))

    rundir = state.run_path(b_id)

    record_details = PerfRecordDetails(
        binary=state.bin_path(b_id),
        record_file=rundir / perf_file,
        saved_record_file=state.root / saved_state_dir / perf_file,
    )
    state.bench_perf_details[b_id] = [record_details]

    if not state.force_rerun and record_details.saved_record_file.exists():
        logger.info(
            "Matching previous profiling file found, skipping %s",
            state.benchmarks[b_id],
        )
        return

    logger.info(
        "Running and profiling benchmark %s…",
        state.benchmarks[b_id],
    )
    try:
        bench_inputs = shlex.split(
            _BENCHMARK_INPUTS[state.benchmarks[b_id]][_ID_FOR_SIZE[state.rodinia_size]]
        )
        subprocess.run(
            instrument_prefix + [state.bin_path(b_id).as_posix()] + bench_inputs,
            cwd=rundir.as_posix(),
            capture_output=True,
            text=True,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to profile/run benchmark %s (status code %d). Output: \n%s",
            state.benchmarks[b_id],
            exn.returncode,
            exn.stderr,
        )
        raise RodiniaBenchException(
            f"Could not profile/run benchmark {state.benchmarks[b_id]}."
        ) from exn

    # Saving profiling data
    record_details.saved_record_file.parent.mkdir(parents=True, exist_ok=True)
    record_details.record_file.rename(record_details.saved_record_file)


@contextmanager
def makefile_disable_bench(makefile: Path, benchs: list[str]):
    if not benchs:
        # Do nothing
        yield
    else:
        # Backup the original Makefile
        makefile_bck = makefile.with_suffix(makefile.suffix + ".bck")
        makefile.rename(makefile_bck)
        openmp_bench_re = re.compile(r"cd openmp/(?P<bench>[^;]+);")
        with makefile.open("w") as handle:
            with makefile_bck.open("r") as orig:
                for line in orig:
                    match = openmp_bench_re.match(line.strip())
                    if match and match.group("bench") in benchs:
                        handle.write("#" + line)
                    else:
                        handle.write(line)
        yield
        makefile_bck.replace(makefile)


def rodinia_build_openmp(state: RodiniaState):
    """Compile benchmarks in openmp mode"""
    try:
        subprocess.run(
            ["make", "OMP"],
            capture_output=True,
            cwd=state.root.as_posix(),
            text=True,
            check=True,
        )
    except subprocess.CalledProcessError as exn:
        logger.critical(
            "Failed to compile Rodinia benchmarks (status code %d). Output:\n%s",
            exn.returncode,
            exn.output,
        )
        raise RodiniaException("Could not compile benchmarks.") from exn


def rodinia_init(state: RodiniaState):
    """Initialize and compile Rodinia"""

    # Find the various available benchmarks
    omp_src_root = state.root / "openmp"
    all_benchs = []
    for src_dir in omp_src_root.iterdir():
        if src_dir.is_dir():
            all_benchs.append(src_dir.name)

    # Cleanup bin dir
    logger.info("Cleaning up previous binaries…")
    omp_run_dir = "bin/linux/omp"
    omp_bin_dir = state.root / omp_run_dir
    for file in omp_bin_dir.iterdir():
        file.unlink()

    # Disable compilation of unwanted benchmarks
    unwanted_benchs: set[str] = set()
    makefile_path = state.root / "Makefile"
    if not state.rodinia_use_mummergpu:
        unwanted_benchs.add("mummergpu")
    if state.rodinia_limit:
        unwanted_benchs.update(set(all_benchs) - set(state.rodinia_limit))
    logger.info("Compiling benchmarks (ignoring: %s)…", ", ".join(unwanted_benchs))
    with makefile_disable_bench(makefile_path, list(unwanted_benchs)):
        # Compile openmp benchmarks
        rodinia_build_openmp(state)

    for b_id, binary in enumerate(omp_bin_dir.iterdir()):
        state.benchmarks[b_id] = binary.name
        state.bench_rundir[b_id] = omp_run_dir
