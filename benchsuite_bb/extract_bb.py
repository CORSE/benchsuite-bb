import logging
import pickle
import subprocess
import typing as t
from collections import defaultdict
from pathlib import Path

from ruamel import yaml
from tbastian_pyutils import basic_blocks

from .bench_state import OutputState, PerfRecordDetails, SuiteState
from .perf_trace import PerfTraceDecoder
from .suite_specific import polybench, rodinia, spec2017

logger = logging.getLogger(__name__)


def profiling_analyze(
    state: SuiteState, b_id: int
) -> tuple[
    dict[basic_blocks.Block, int], dict[basic_blocks.Block, basic_blocks.BlockData]
]:
    """Analyzes the profiling data of the previous runs of benchmarks included in a
    single benchmark id, returning a mapping from sampled basic blocks to their number
    of sampled occurences."""

    logger.info(
        "Extracting samples from perf record script for %s...", state.benchmarks[b_id]
    )
    # Assumption: all runs with shared b_id have the same binary. It seems to be true
    # and makes the code simpler.
    binary = state.bench_perf_details[b_id][0].binary
    assert all([details.binary == binary for details in state.bench_perf_details[b_id]])
    trace_decoder = PerfTraceDecoder(binary)

    samples: list[int] = []
    for details in state.bench_perf_details[b_id]:
        trace = trace_decoder.trace_from_perfdata(
            details.saved_record_file, perf_bin=state.perf_bin
        )
        samples += trace_decoder.analyze_trace(trace)

    logger.info(
        "%s: %d samples. Mapping to basic blocks...",
        state.benchmarks[b_id],
        len(samples),
    )

    block_occur: dict[basic_blocks.Block, int] = defaultdict(int)
    bb_dec = basic_blocks.BBDecoder(binary)
    blockdata: dict[basic_blocks.Block, basic_blocks.BlockData] = {}

    for sample in samples:
        try:
            block = bb_dec.block_at(sample)
            block_occur[block] += 1
            if block not in blockdata:
                blockdata[block] = bb_dec.get_block_data(
                    block, disasm=state.need_disasm
                )
        except basic_blocks.AddressInNonTextCode:  # Benign
            pass
        except basic_blocks.BasicBlockException as exn:
            logger.warning(
                "Could not use sample at 0x%x: %s %s",
                sample,
                exn.__class__.__name__,
                exn,
            )

    return block_occur, blockdata


def dump_blocks(
    state: SuiteState,
    b_id: int,
    occurs: dict[basic_blocks.Block, int],
    blockdata: dict[basic_blocks.Block, basic_blocks.BlockData],
    output_state: OutputState,
):
    """Dumps basic blocks for a given benchmark"""

    filtered_out: set[int] = set()

    # Relative filtering
    if output_state.filter_relative is not None:
        total_occurs = sum(occurs.values())
        threshold = (output_state.filter_relative / 100) * total_occurs
        for blk_id, occur in enumerate(occurs.values()):
            if occur < threshold:
                filtered_out.add(blk_id)

    # Absolute filtering
    if output_state.filter_absolute is not None:
        for blk_id, occur in enumerate(occurs.values()):
            if occur < output_state.filter_absolute:
                filtered_out.add(blk_id)

    if output_state.dump_tb:
        with (output_state.out_dir / (state.benchmarks[b_id] + ".tb")).open(
            "w"
        ) as handle:
            for blk_id, (block, occur) in enumerate(occurs.items()):
                if blk_id in filtered_out:
                    continue
                data = blockdata[block]
                assert data.disasm is not None
                handle.write(f"TB 0x{block.addr:08x} was executed {occur} time(s)\n")
                for insn in data.disasm:
                    insn_bytes = []
                    insn_hex = insn.bytes.hex()
                    assert len(insn_hex) % 2 == 0
                    for pos in range(0, len(insn_hex), 2):
                        insn_bytes.append(insn_hex[pos : pos + 2])
                    insn_bytestxt = " ".join(insn_bytes)

                    insn_disasm = f"[[{insn.mnemonic} {insn.op_str}]]"

                    handle.write(f"{insn_bytestxt}\t{insn_disasm}\n")

                handle.write("End of TB\n")

    if output_state.dump_yml:
        yaml_data = []
        for blk_id, (block, occur) in enumerate(occurs.items()):
            if blk_id in filtered_out:
                continue
            data = blockdata[block]
            yaml_data.append(
                {
                    "addr": block.addr,
                    "size": block.size,
                    "bytes": data.raw.hex(),
                    "occur": occur,
                }
            )

        with (output_state.out_dir / (state.benchmarks[b_id] + ".yml")).open(
            "w"
        ) as handle:
            yaml.dump(yaml_data, handle)


def extract_all(
    state: SuiteState,
    output_state: OutputState,
):
    """Entrypoint function that extracts basic blocks"""

    state.need_disasm = output_state.need_disasm

    if isinstance(state, spec2017.SpecState):
        logger.info("Initializing SPEC2017 test suite…")
        spec2017.spec_init(state)
        logger.info("Selected benchmarks: %s", ", ".join(state.benchmarks.values()))

        for b_id in state.benchmarks:
            logger.info("Processing benchmark %s…", state.benchmarks[b_id])
            # Execute benchmarks
            spec2017.spec_bench(state, b_id)
    elif isinstance(state, polybench.PolybenchState):
        logger.info("Initializing Polybench test suite…")
        polybench.polybench_init(state)
        logger.info("Selected benchmarks: %s", ", ".join(state.benchmarks.values()))

        for b_id in state.benchmarks:
            logger.info("Processing benchmark %s…", state.benchmarks[b_id])
            # Execute benchmarks
            polybench.polybench_bench(state, b_id)
    elif isinstance(state, rodinia.RodiniaState):
        logger.info("Initializing Rodinia test suite…")
        rodinia.rodinia_init(state)
        logger.info("Selected benchmarks: %s", ", ".join(state.benchmarks.values()))

        for b_id in state.benchmarks:
            logger.info("Processing benchmark %s…", state.benchmarks[b_id])
            # Execute benchmarks
            rodinia.rodinia_bench(state, b_id)

    bench_occurs = {}
    for b_id in state.benchmarks:
        logger.info("Analyzing benchmark %s…", state.benchmarks[b_id])
        # Analyze benchmarks
        bench_occurs[b_id], bench_data = profiling_analyze(state, b_id)

        # Dump results
        dump_blocks(state, b_id, bench_occurs[b_id], bench_data, output_state)
