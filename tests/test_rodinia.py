import tempfile
import typing as t
from pathlib import Path

from benchsuite_bb.suite_specific import rodinia


def test_makefile_disable_bench():
    test_input = """OMP:
    cd openmp/foo; foo
    cd openmp/bar; bar
    cd openmp/baz; baz
    cd opencl/foo; foo
    cd openmp/foo; foo """
    lines_affected: dict[str, list[int]] = {
        "foo": [1, 5],
        "bar": [2],
        "baz": [3],
    }

    def get_lines_affected(path: Path) -> list[int]:
        out = []
        with path.open("r") as handle:
            for l_id, line in enumerate(handle):
                if line.startswith("#"):
                    out.append(l_id)
        return out

    with tempfile.TemporaryDirectory() as raw_tmpdir:
        tmpdir = Path(raw_tmpdir)
        tmpfile = tmpdir / "Makefile"
        with tmpfile.open("w") as handle:
            handle.write(test_input)

        for ignored_set in [["foo"], ["foo", "bar"]]:
            assert get_lines_affected(tmpfile) == []
            with rodinia.makefile_disable_bench(tmpfile, ignored_set):
                should_affect: set[int] = set()
                for elt in ignored_set:
                    should_affect.update(lines_affected[elt])
                assert get_lines_affected(tmpfile) == list(should_affect)
            assert get_lines_affected(tmpfile) == []
