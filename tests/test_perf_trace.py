import typing as t

from benchsuite_bb import perf_trace


def test_analyze():
    """Test analyze with a trivial binary"""
    dec: perf_trace.PerfTraceDecoder = perf_trace.PerfTraceDecoder.new_bare()
    dec.segments.append(perf_trace.Segment(addr=0, size=0x100))

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x0(0x200) @ 0 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x50
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x99
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x100
"""
        )
        == [0, 0, 0x50, 0x99]
    )  # 0x100 is outside of the segment

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x0(0x50) @ 0 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x49
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x50
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x100
"""
        )
        == [0, 0, 0x49]
    )  # 0x50 is outside the loaded region

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x1000(0x100) @ 0 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x100
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x999
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1000
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1050
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1099
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1100
"""
        )
        == [0, 0x50, 0x99]
    )  # Offset by 0x1000

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x0(0x50) @ 0x50 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x20
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x49
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x100
"""
        )
        == [0x50, 0x70, 0x99]
    )  # Offset in exe of 0x50

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x1000(0x50) @ 0x50 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x999
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1000
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1020
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1049
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1100
"""
        )
        == [0x50, 0x70, 0x99]
    )  # Offset in exe of 0x50

    assert (
        dec.analyze_trace(
            """
PERF_RECORD_MMAP2 9798/9798: [0x0(0x100) @ 0 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x0
PERF_RECORD_EXIT(9798:9798):(9798:9798)
PERF_RECORD_MMAP2 9798/9798: [0x1000(0x100) @ 0 00:00 0 0]: r-xp /some/file
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x10
PERF_RECORD_SAMPLE(IP, 0x4002): 9798/9798: 0x1020
"""
        )
        == [0x0, 0x20]
    )  # Opened twice
